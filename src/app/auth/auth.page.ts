import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

import { LoadingController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';

import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  public actionType: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.actionType = 'login';
  }

  onLogin() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Logging in...'})
      .then((loadingEl) => {
        loadingEl.present();
        setTimeout(() => {
          loadingEl.dismiss();
          this.router.navigateByUrl('/places/tabs/discover');
        }, 1500);
      });
    this.authService.login();
  }

  onActionTypeUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    this.actionType = event.detail.value;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    if (this.actionType === 'login') {
      // Login user
    } else if (this.actionType === 'register') {
      // Register user
    }
    console.log(form.value);
    this.onLogin();
  }

}
