import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ModalController, ActionSheetController, LoadingController, AlertController } from '@ionic/angular';
import { take } from 'rxjs/operators';

import { NewBookingComponent } from '../../../bookings/new-booking/new-booking.component';
import { PlacesService } from '../../places.service';
import { BookingsService } from '../../../bookings/bookings.service';
import { AuthService } from '../../../auth/auth.service';

import { Place } from '../../place.model';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit {
  public place: Place;
  public isBookable = false;
  public isLoading = false;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private placesService: PlacesService,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private bookingsService: BookingsService,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (paramMap) => {
        if (!paramMap.has('placeId')) {
          this.navCtrl.navigateBack('/places/tabs/offers');
          return;
        }
        this.isLoading = true;
        this.placesService.getPlace(paramMap.get('placeId')).pipe(take(1)).subscribe(
          (place) => {
            this.place = place;
            this.isBookable = place.userId !== this.authService.userId;
            this.isLoading = false;
          },
          (error) => {
            this.alertCtrl.create({
              header: 'An error occurred!',
              message: 'Could not load place.',
              buttons: [
                {text: 'Okay', handler: () => this.router.navigateByUrl('/places/tabs/discover')}
              ]
            })
            .then(alertEl => alertEl.present());
          }
        );
      }
    );
  }

  onBookPlace() {
    this.actionSheetCtrl.create({
      header: 'Choose an Action',
      buttons: [
        {
          text: 'Select Date',
          handler: () => {
            this.openBookingModal('select');
          }
        },
        {
          text: 'Random Date',
          handler: () => {
            this.openBookingModal('random');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    })
      .then((actionSheetEl) => {
      actionSheetEl.present();
      });
  }

  openBookingModal(mode: 'select' | 'random') {
    console.log(mode);
    this.modalCtrl
    .create({ component: NewBookingComponent, componentProps: { selectedPlace: this.place, selectedMode: mode } })
    .then((modal) => {
      modal.present();
      return modal.onDidDismiss();
    })
    .then((dismissData) => {
      if (dismissData.role === 'confirm') {
        this.loadingCtrl
          .create({message: 'Booking place...'})
          .then((loadingEl) => {
            loadingEl.present();
            const data = dismissData.data.bookingData;
            this.bookingsService.addBooking(
              this.place.id,
              this.place.title,
              this.place.imageUrl,
              data['first-name'],
              data['last-name'],
              +data['guests-number'],
              new Date(data['date-from']),
              new Date(data['date-until'])
            ).subscribe(() => {
              loadingEl.dismiss();
            });
          });
      }
    });
  }
}
