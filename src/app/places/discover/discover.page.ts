import { Component, OnInit, OnDestroy } from '@angular/core';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription } from 'rxjs';

import { PlacesService } from '../places.service';
import { AuthService } from '../../auth/auth.service';

import { Place } from '../place.model';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit, OnDestroy {
  private filter = 'all';
  private placesSub: Subscription;
  public loadedPlaces: Place[];
  public relevantPlaces: Place[];
  public isLoading: boolean;

  constructor(private placesService: PlacesService, private authService: AuthService) { }

  ngOnInit() {
    this.placesSub = this.placesService.places.subscribe(
      (places) => {
        this.loadedPlaces = places;
        if (this.filter === 'bookable') {
          this.relevantPlaces = this.loadedPlaces.filter(
            place => place.userId !== this.authService.userId
          );
        } else {
          this.relevantPlaces = this.loadedPlaces;
        }
      }
    );
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.placesService.fetchPlaces().subscribe(() => {
      this.isLoading = false;
    });
  }

  onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    if (event.detail.value === 'all') {
      this.relevantPlaces = this.loadedPlaces;
      this.filter = 'all';
    } else {
      this.relevantPlaces = this.loadedPlaces.filter(
        place => place.userId !== this.authService.userId
      );
      this.filter = 'bookable';
    }
  }

  ngOnDestroy() {
    this.placesSub.unsubscribe();
  }

}
