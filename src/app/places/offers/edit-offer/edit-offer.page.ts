import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { PlacesService } from '../../places.service';

import { Place } from '../../place.model';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit, OnDestroy {
  private placeSub: Subscription;
  public offer: Place;
  public offerForm: FormGroup;
  public isLoading = false;
  public placeId: string;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private placesService: PlacesService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (paramMap) => {
        if (!paramMap.has('placeId')) {
          this.navCtrl.navigateBack('/places/tabs/offers');
          return;
        }
        this.placeId = paramMap.get('placeId');
        this.isLoading = true;
        this.placeSub = this.placesService.getPlace(paramMap.get('placeId')).subscribe(
          (place) => {
            this.offer = place;
            this.offerForm = new FormGroup({
              'title': new FormControl(this.offer.title, {updateOn: 'blur', validators: [Validators.required]}),
              'description': new FormControl(this.offer.description, {
                updateOn: 'blur', validators: [Validators.required, Validators.maxLength(200)]
              }),
              'price': new FormControl(this.offer.price, {updateOn: 'blur', validators: [Validators.required, Validators.min(1)]}),
              'dateFrom': new FormControl(this.offer.availableFrom.toISOString(), {updateOn: 'blur', validators: Validators.required}),
              'dateUntil': new FormControl(this.offer.availableUntil.toISOString(), {updateOn: 'blur', validators: Validators.required})
            });
            this.isLoading = false;
          },
          (error) => {
            this.alertCtrl.create({
              header: 'An error occurred!',
              message: 'Place could not be fetched. Please try again later.',
              buttons: [
                {text: 'Okay', handler: () => {this.router.navigateByUrl('/places/tabs/offers')} }
              ]
            })
            .then(alertEl => alertEl.present());
          }
        );

      }
    );
  }

  onupdatePlace() {
    if (!this.offerForm.valid) {
      return;
    }
    this.loadingCtrl.create({
      message: 'Saving offer...'
    }).then((loadingEl) => {
      loadingEl.present();
      this.placesService.updatePlace(
        this.offer.id,
        this.offerForm.value.title,
        this.offerForm.value.description,
        +this.offerForm.value.price,
        new Date(this.offerForm.value.dateFrom),
        new Date(this.offerForm.value.dateUntil)
      ).subscribe(() => {
        loadingEl.dismiss();
        this.offerForm.reset();
        this.router.navigate(['places/tabs/offers']);
      });
    });
  }

  ngOnDestroy() {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }
}
