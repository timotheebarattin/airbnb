import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PlacesService } from '../places.service';

import { Place } from '../place.model';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  public isLoading = false;
  private placesSub: Subscription;
  loadedOffers: Place[] = [];

  constructor(private placesService: PlacesService, private router: Router) { }

  ngOnInit() {
    this.placesSub = this.placesService.places.subscribe(
      (places) => {
        this.loadedOffers = places;
      }
    );
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.placesService.fetchPlaces().subscribe(
      () => {
        this.isLoading = false;
      }
    );
  }

  onEdit(offerId: string, slidingItemRef: IonItemSliding) {
    slidingItemRef.close();
    this.router.navigateByUrl('/places/tabs/offers/edit/' + offerId);
  }

  onDelete() {
    console.log('Delete offer');
  }

  ngOnDestroy() {
    if (this.placesSub) {
      this.placesSub.unsubscribe();
    }
  }

}
