import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, of } from "rxjs";
import { take, map, tap, delay, switchMap } from "rxjs/operators";

import { AuthService } from "../auth/auth.service";

import { Place } from "./place.model";

interface PlaceData {
  availableFrom: string;
  availableUntil: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
}

// [
// new Place(
// 'p1',
// 'Sotto la Mole in due',
// 'Romantic under-the-roof apartment in front of the Mole Antonelliana of Torino',
// 'https://s-ec.bstatic.com/images/hotel/max1280x900/956/95642025.jpg',
// 80,
// new Date('2019-01-01'),
// new Date('2019-12-31'),
// 'abc'
// ),
// new Place(
// 'p2',
// 'Junto a Nuestra Señora del Pilar',
// 'Cosy apartment in Zaragoza\'s old town with a view on the Basilica',
// 'https://res.cloudinary.com/lastminute/image/upload/c_scale,w_630/q_auto/v1534729489/zw5un06lmznljdvpgfad.jpg',
// 70,
// new Date('2019-01-01'),
// new Date('2019-12-31'),
// 'xyz'
// ),
// new Place(
// 'p3',
// 'Am Zürisee',
// 'Beautiful and luminous apartment on the shore of lake Zurich',
// 'https://media-cdn.tripadvisor.com/media/photo-s/10/06/36/f2/lake-view-executive-room.jpg',
// 200,
// new Date('2019-01-01'),
// new Date('2019-12-31'),
// 'abc'
// ),
// ]

@Injectable({
  providedIn: "root"
})
export class PlacesService {
  private _places = new BehaviorSubject<Place[]>([]);

  get places() {
    return this._places.asObservable();
  }

  constructor(private authService: AuthService, private http: HttpClient) {}

  fetchPlaces() {
    return this.http.get<Place[]>("http://localhost:3000/places").pipe(
      tap((fetchedPlaces: Place[]) => {
        this._places.next(fetchedPlaces);
      })
    );
  }

  getPlace(id: string) {
    return this.http.get<Place>(`http://localhost:3000/places/${id}`);
  }

  addPlace(
    title: string,
    description: string,
    price: number,
    dateFrom: Date,
    dateUntil: Date
  ) {
    let newPlace: Place;
    const newPlaceData = {
      title: title,
      description: description,
      imageUrl:
        "http://projets.cotemaison.fr/uploads/projects/2067/project_940774_pic_1.jpg",
      price: price,
      availableFrom: dateFrom,
      availableUntil: dateUntil,
      userId: this.authService.userId
    };
    return this.http
      .post<Place>("http://locahost:3000/places", { ...newPlaceData })
      .pipe(
        switchMap((addedPlace: Place) => {
          newPlace = addedPlace;
          return this.places;
        }),
        tap((places: Place[]) => {
          this._places.next(places.concat(newPlace));
        })
      );
  }

  updatePlace(
    id: string,
    title: string,
    description: string,
    price: number,
    dateFrom: Date,
    dateUntil: Date
  ) {
    let updatedPlace: Place;
    const updatedPlaceData = {
      title: title,
      description: description,
      price: price,
      availableFrom: dateFrom,
      availablUntil: dateUntil
    };
    return this.http
      .patch<Place>(`http://localhost:3000/places/${id}`, {
        ...updatedPlaceData
      })
      .pipe(
        switchMap((upToDatePlace: Place) => {
          updatedPlace = upToDatePlace;
          return this.places;
        }),
        tap((places: Place[]) => {
          const updatedPlaceIndex = places.findIndex(
            place => updatedPlace.id === place.id
          );
          if (updatedPlaceIndex >= 0) {
            places[updatedPlaceIndex] = updatedPlace;
          } else if (updatedPlaceIndex < 0) {
            places.push(updatedPlace);
          }
          this._places.next(places);
        }),
        switchMap((oldPlaces: Place[]) => {
          return of(updatedPlace);
        })
      );
  }

  deletePlace(id: string) {
    return this.http.delete(`http://localhost:3000/places/${id}`).pipe(
      switchMap(() => {
        return this.places;
      }),
      tap((places: Place[]) => {
        const deletedPlaceIndex = places.findIndex(place => id === place.id);
        if (deletedPlaceIndex >= 0) {
          places.splice(deletedPlaceIndex, 1);
        }
        this._places.next(places);
      }),
      switchMap((oldPlaces: Place[]) => {
        return this.places;
      })
    );
  }
}
