import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ModalController } from '@ionic/angular';

import { Place } from '../../places/place.model';

@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.scss'],
})
export class NewBookingComponent implements OnInit {
  @Input() selectedPlace: Place;
  @Input() selectedMode: 'select' | 'random';
  public startDate: string;
  public endDate: string;

  @ViewChild('formContent') bookingForm: NgForm;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    const availableFrom = new Date(this.selectedPlace.availableFrom);
    const availableUntil = new Date(this.selectedPlace.availableUntil);
    
    if (this.selectedMode === 'random') {
      this.startDate = new Date(
        ( availableUntil.getTime() - availableFrom.getTime() - 7 * 24 * 60 * 60 * 1000 ) * Math.random() + availableFrom.getTime()
      ).toISOString();

      this.endDate = new Date(
        ( 7 * 24 * 60 * 60 * 1000 ) * Math.random() + new Date(this.startDate).getTime()
      ).toISOString();
    }
  }

  datesValid() {
    const startDate = new Date(this.bookingForm.value['date-from']);
    const endDate = new Date(this.bookingForm.value['date-until']);
    return endDate > startDate;
  }

  onBookPlace() {
    if (!this.bookingForm.valid || !this.datesValid()) {
      return;
    }
    console.log(this.bookingForm.value);
    this.modalCtrl.dismiss({'bookingData': this.bookingForm.value}, 'confirm');
  }

  onCancel() {
    this.modalCtrl.dismiss();
  }

}
