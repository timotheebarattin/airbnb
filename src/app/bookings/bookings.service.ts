import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import {take, delay, tap, switchMap, map } from 'rxjs/operators';

import { AuthService } from '../auth/auth.service';

import { Booking } from './booking.model';

interface BookingData {
    placeId: string;
    userId: string;
    placeTitle: string;
    placeImage: string;
    firstName: string;
    lastName: string;
    guestsNumber: number;
    bookedFrom: Date;
    bookedUntil: Date;
}

@Injectable({
    providedIn: 'root'
})
export class BookingsService {
    private _bookings = new BehaviorSubject<Booking[]>([]);

    get bookings() {
        return this._bookings.asObservable();
    }

    constructor(private authService: AuthService, private http: HttpClient) {}

    fetchBookings() {
        return this.http.get<{ [key: string]: BookingData }>(`https://ionic-angular-airbnb.firebaseio.com/bookings.json?orderBy="userId"&equalTo="${this.authService.userId}"`).pipe(
            map((resData) => {
                const bookings: Booking[] = [];
                for (const key in resData) {
                    if (resData.hasOwnProperty(key)) {
                        bookings.push(
                            new Booking(
                                key,
                                resData[key].placeId,
                                resData[key].userId,
                                resData[key].placeTitle,
                                resData[key].placeImage,
                                resData[key].firstName,
                                resData[key].lastName,
                                resData[key].guestsNumber,
                                new Date(resData[key].bookedFrom),
                                new Date(resData[key].bookedUntil)
                            )
                        );
                    }
                }
                return bookings;
            }),
            tap(bookings => {
                this._bookings.next(bookings);
            })
        );
    }

    addBooking(
        placeId: string,
        placeTitle: string,
        placeImage: string,
        firstName: string,
        lastName: string,
        guestsNumber: number,
        dateFrom: Date,
        dateUntil: Date
    ) {
        let bookingId: string;
        const newBooking = new Booking(
            Math.random().toString(),
            placeId,
            this.authService.userId,
            placeTitle,
            placeImage,
            firstName,
            lastName,
            guestsNumber,
            dateFrom,
            dateUntil
        );
        return this.http.post<{name: string}>('https://ionic-angular-airbnb.firebaseio.com/bookings.json', { ...newBooking, id: null }).pipe(
            switchMap((response) => {
                bookingId = response.name;
                return this.bookings;
            }),
            take(1),
            tap(bookings => {
                newBooking.id = bookingId;
                this._bookings.next(bookings.concat(newBooking));
            })
        );
    }

    cancelBooking(bookingId: string) {
        return this.http.delete(`https://ionic-angular-airbnb.firebaseio.com/bookings/${bookingId}.json`).pipe(
            switchMap(() => {
                return this.bookings;
            }),
            take(1),
            tap(bookings => {
                this._bookings.next(bookings.filter(b => b.id !== bookingId));
            })
        );
    }
}