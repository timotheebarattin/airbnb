import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonItemSliding, LoadingController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { BookingsService } from './bookings.service';

import { Booking } from './booking.model';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
  private bookingsSub: Subscription;
  public loadedBookings: Booking[];
  public isLoading = true;

  constructor(private bookingsService: BookingsService, private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.bookingsSub = this.bookingsService.bookings.subscribe(
      bookings => this.loadedBookings = bookings
    );
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.bookingsService.fetchBookings().subscribe(() => {
      this.isLoading = false;
    })
  }

  onCancelBooking(bookingId: string, slidingItemRef: IonItemSliding) {
    this.loadingCtrl
      .create({message: 'Cancelling booking...'})
      .then((loadingEl) => {
        loadingEl.present();
        this.bookingsService.cancelBooking(bookingId).subscribe(
          () => {
            loadingEl.dismiss();
            slidingItemRef.close();
          }
        );
      });
  }

  ngOnDestroy() {
    this.bookingsSub.unsubscribe();
  }

}
